import {combineReducers} from 'redux';
import chatReducer from '../containers/Chat/reducer';
import editMessageReducer from '../components/EditMessage/reducer';
import profile from '../containers/LoginPage/reducer';

export default combineReducers({chat: chatReducer, modal: editMessageReducer, profile});
