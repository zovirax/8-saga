import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from 'redux-saga';
import rootReducer from "../reducers";
import thunk from 'redux-thunk';
import rootSaga from '../sagas/index';

export default function configureStore() {
    const sagaMiddleware = createSagaMiddleware();
    const store = createStore(
        rootReducer,
        applyMiddleware(sagaMiddleware, thunk)
    );

    sagaMiddleware.run(rootSaga)

    return store;
}
