export const login = async ({email: user, password}) => {
    const response = await fetch('http://localhost:5000/api/auth/login', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({user, password})
    })
    return response.json();
};


export const getCurrentUser = async () => {
    try {
        const response = await fetch('http://localhost:5000/api/auth/profile', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        return response.json();
    } catch (e) {
        return null;
    }
};
