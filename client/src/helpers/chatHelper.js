export const getLastMessageByDate = (messages = []) => messages.slice(-1)[0].createdAt;

export const getMemberCount = (messages = []) => {
    const senderUsernames = messages.map(m => m.userId);
    return new Set(senderUsernames).size;
};
