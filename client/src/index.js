import React from 'react';
import ReactDOM from 'react-dom';
import Routing from './containers/Routing';
import Chat from './containers/Chat/index';
import NotFound from './components/NotFound';
import EditMessage from './components/EditMessage/index';
import configureStore from './store/configureStore';
import {Provider} from 'react-redux';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import './styles/reset.css';
import './styles/common.css';

const store = configureStore();

const root = document.getElementById('root');
ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <Router>
                <Routing/>
            </Router>
        </Provider>
    </React.StrictMode>,
    root);
