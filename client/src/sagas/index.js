import { all } from 'redux-saga/effects';
import chatSagas from '../containers/Chat/sagas';

export default function* rootSaga() {
    yield all([
        chatSagas()
    ])
};
