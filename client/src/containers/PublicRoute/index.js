import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class PublicRoute extends React.Component {
    render() {
        const {component: Component, isAuthorized, ...rest} = this.props;
        return (
            <Route
                {...rest}
                render={props => (isAuthorized
                    ? <Redirect to={{pathname: '/', state: {from: props.location}}}/>
                    : <Component {...props} />)}
            />
        );
    }
}

PublicRoute.propTypes = {
    isAuthorized: PropTypes.bool,
    component: PropTypes.any.isRequired,
    location: PropTypes.any,
};

PublicRoute.defaultProps = {
    isAuthorized: false,
    location: undefined
};

const mapStateToProps = rootState => ({
    isAuthorized: rootState.profile.isAuthorized
});

export default connect(mapStateToProps)(PublicRoute);
