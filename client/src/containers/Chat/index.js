import React from 'react';
import {connect} from 'react-redux';
import ChatHeader from '../../components/ChatHeader/index';
import MessageList from '../../components/MessageList';
import MessageInput from '../../components/MessageInput';
import Spinner from '../../components/Spinner';
import {getLastMessageByDate, getMemberCount} from '../../helpers/chatHelper';
import * as chatActions from './actions';
import {showModal, setCurrentCommentId} from '../../components/EditMessage/actions';

class Chat extends React.Component {
    sendMessage = (text = '') => {
        this.props.addMessage({
            text,
            userId: this.state.userId,
            user: 'Me',
            createdAt: new Date().toISOString(),
            editedAt: '',
        });
    };

    editMessage = (id = '') => {
        this.props.setCurrentCommentId(id);
        this.props.showModal();
    };

    deleteMessage = (id = '') => {
        this.props.deleteMessage(id);
    };

    componentDidMount() {
        this.setState({user: 'Me'});
        this.props.fetchMessages();
    }

    mapMessage = message => ({
        ...message,
        isOwn: message.user === this.state.user,
        isNew: new Date() - new Date(message.createdAt) < 1e3
    })

    render() {
        const messages = this.props.messages;
        return (
            <div style={{padding: '0 200px 0', position: 'relative'}}>
                {messages.length ?
                    <>
                        <ChatHeader chatName={'BSA Chat'}
                                    membersCount={getMemberCount(messages)}
                                    messagesCount={messages.length}
                                    lastMessageTime={getLastMessageByDate(messages)}/>
                        <MessageList messages={messages.map(msg => this.mapMessage(msg))}
                                     editMessage={this.editMessage}
                                     deleteMessage={this.deleteMessage}/>
                        <MessageInput sendMessage={this.sendMessage}/>}
                    </> : <Spinner/>}
            </div>
        );
    }
}

const mapDispatchToProps = {
    showModal,
    setCurrentCommentId,
    ...chatActions,
}

const mapStateToProps = state => ({
    messages: state.chat
})

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
