import axios from 'axios';
import {call, put, takeEvery, all} from 'redux-saga/effects';
import {ITEMS_FETCH_DATA_SUCCESS, ITEMS_FETCH, ADD_MESSAGE, DELETE_MESSAGE, EDIT_MESSAGE} from "./actionTypes";

const api = {url: 'http://localhost:5000/api/messages'};

export function* fetchMessages() {
    try {
        const messages = yield call(axios.get, api.url);
        yield put({type: ITEMS_FETCH_DATA_SUCCESS, payload: {messages: messages.data}})
    } catch (error) {
        console.log('fetchMessages error:', error.message)
    }
}

function* watchFetchMessages() {
    yield takeEvery(ITEMS_FETCH, fetchMessages)
}

export function* addMessage(action) {
    const newMessage = {...action.payload.data, id: action.payload.id};

    try {
        yield call(axios.post, api.url, newMessage);
        yield put({type: ITEMS_FETCH});
    } catch (error) {
        console.log('addMessage error:', error.message);
    }
}

function* watchAddMessage() {
    yield takeEvery(ADD_MESSAGE, addMessage)
}

export function* updateMessage(action) {
    const id = action.payload.id;
    const updatedMessage = {...action.payload.data};

    try {
        yield call(axios.put, `${api.url}/${id}`, updatedMessage);
        yield put({type: ITEMS_FETCH});
    } catch (error) {
        console.log('updateUser error:', error.message);
    }
}

function* watchUpdateMessage() {
    yield takeEvery(EDIT_MESSAGE, updateMessage)
}

export function* deleteMessage(action) {
    try {
        yield call(axios.delete, `${api.url}/${action.payload.id}`);
        yield put({type: ITEMS_FETCH})
    } catch (error) {
        console.log('deleteUser Error:', error.message);
    }
}

function* watchDeleteMessage() {
    yield takeEvery(DELETE_MESSAGE, deleteMessage)
}

export default function* () {
    yield all([
        watchFetchMessages(),
        watchAddMessage(),
        watchDeleteMessage(),
        watchUpdateMessage()
    ])
};
