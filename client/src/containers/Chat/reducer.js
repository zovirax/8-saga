import { ITEMS_FETCH_DATA_SUCCESS} from './actionTypes';

export default (state = [], action) => {
    switch (action.type) {
        case ITEMS_FETCH_DATA_SUCCESS:
            return action.payload.messages;
        default:
            return state;
    }
}
