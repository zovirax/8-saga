import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class PrivateRoute extends React.Component {
    render() {
        let {component: Component, isAuthorized, ...rest} = this.props;
        return (
            <Route
                {...rest}
                render={props => (isAuthorized
                    ? <Component {...props} />
                    : <Redirect to={{pathname: '/login', state: {from: props.location}}}/>)}
            />
        );
    }
}

PrivateRoute.propTypes = {
    isAuthorized: PropTypes.bool,
    component: PropTypes.any.isRequired,
    location: PropTypes.any,
};

PrivateRoute.defaultProps = {
    isAuthorized: false,
    location: undefined
};

const mapStateToProps = rootState => ({
    isAuthorized: rootState.profile.isAuthorized
});

export default connect(mapStateToProps)(PrivateRoute);
