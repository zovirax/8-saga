import React, {useEffect} from 'react';
import {Route, Switch} from 'react-router-dom';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import LoginPage from '../../containers/LoginPage';
import Header from '../../components/Header';
import Spinner from '../../components/Spinner';
import NotFound from '../../components/NotFound';
import PrivateRoute from '../PrivateRoute';
import PublicRoute from '../PublicRoute';
import {loadCurrentUser} from '../LoginPage/actions';
import Chat from '../Chat';
import PropTypes from 'prop-types';

class Routing extends React.Component {
    componentDidMount() {
        const {isAuthorized, loadCurrentUser: loadUser} = this.props;
        if (!isAuthorized) {
            loadUser();
        }
    }

    render() {
        let {
            isLoading
        } = this.props;

        return (
            (
                <div className="fill">
                    <main className="fill">
                        <Switch>
                            <PublicRoute exact path="/login" component={LoginPage}/>
                            <PrivateRoute exact path="/" component={Chat}/>
                            <Route path="*" exact component={NotFound}/>
                        </Switch>
                    </main>
                </div>
            )
        );
    }
}

Routing.propTypes = {
    isAuthorized: PropTypes.bool,
    user: PropTypes.objectOf(PropTypes.any),
    isLoading: PropTypes.bool,
    loadCurrentUser: PropTypes.func.isRequired
};

Routing.defaultProps = {
    isAuthorized: false,
    user: {},
    isLoading: true
};

const actions = {loadCurrentUser};

const mapStateToProps = ({profile}) => ({
    isAuthorized: profile.isAuthorized,
    user: profile.user,
    isLoading: profile.isLoading
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Routing);
