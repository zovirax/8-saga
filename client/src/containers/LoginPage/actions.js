import * as authHelper from '../../helpers/authHelper';
import { SET_USER } from './actionTypes';

const setToken = token => localStorage.setItem('token', token);

const setUser = user => async dispatch => dispatch({
    type: SET_USER,
    user
});

const setAuthData = (user = null, token = '') => (dispatch, getRootState) => {
    setToken(token); // token should be set first before user
    setUser(user)(dispatch, getRootState);
};

const handleAuthResponse = authResponsePromise => async (dispatch, getRootState) => {
    const { user, token } = await authResponsePromise;
    setAuthData(user, token)(dispatch, getRootState);
};

export const login = request => handleAuthResponse(authHelper.login(request));

export const loadCurrentUser = () => async (dispatch, getRootState) => {
    const user = await authHelper.getCurrentUser();
    setUser(user)(dispatch, getRootState);
};
