import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Header, Message } from 'semantic-ui-react';
import { login } from './actions';
import LoginForm from '../../components/LoginForm';
import styles from './styles.module.css';
import {bindActionCreators} from "redux";

const {connect} = require("react-redux");

class LoginPage extends React.Component {
    render() {
        let {login: signIn} = this.props;
        return (
            <Grid textAlign="center" verticalAlign="middle" className={styles.container}>
                <Grid.Column style={{maxWidth: 450}}>
                    <Header as="h2" color="teal" textAlign="center">
                        Login to your account
                    </Header>
                    <LoginForm login={signIn}/>
                    <Message>
                        All rights reserved. 2005
                    </Message>
                </Grid.Column>
            </Grid>
        );
    }
}

LoginPage.propTypes = {
    login: PropTypes.func.isRequired
};


const actions = { login };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    null,
    mapDispatchToProps
)(LoginPage);
