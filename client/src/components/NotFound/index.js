import React from 'react';

import styles from './styles.module.css';

class NotFound extends React.Component {
    render() {
        return (
            <div className={styles.container}>
                <p>Ooops... Resource Not Found</p>
            </div>
        );
    }
}

export default NotFound;
