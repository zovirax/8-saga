import React from 'react';
import {connect} from 'react-redux';
import * as modalActions from './actions';
import {editMessage} from '../../containers/Chat/actions';
import styles from './styles.module.css';

class EditMessage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.messageId) {
            const message = this.props.messages.find(msg => msg.id === nextProps.messageId);
            this.setState(message);
        }
    }

    onChange = ({target: {value}}) => {
        this.setState({...this.state, text: value});
    }

    handleSubmit = () => {
        this.props.editMessage(this.props.messageId, {
            ...this.state,
            editedAt: new Date().toISOString()
        });
        this.props.dropCurrentCommentId();
        this.props.hideModal()
    }

    onCancel = () => {
        this.props.dropCurrentCommentId();
        this.props.hideModal();
    }

    getModalContent = () =>
        (<div className={styles.modal_container}>
            <div className={styles.modal_content}>
                <header><h3>Edit message</h3></header>
                <section>
                    <textarea
                        value={this.state.text}
                        onChange={this.onChange}
                        cols="30"
                        rows="8"
                        autoFocus/>
                    <div className={styles.modal_footer}>
                        <button onClick={this.handleSubmit}>OK</button>
                        <button onClick={this.onCancel}>Cancel</button>
                    </div>
                </section>
            </div>
        </div>);

    render() {
        return this.props.isShown ? this.getModalContent() : null;
    }
}

const mapStateToProps = state => ({
    isShown: state.modal.isShown,
    messageId: state.modal.messageId,
    messages: state.chat,
});

const mapDispatchToProps = ({...modalActions, editMessage});

export default connect(mapStateToProps, mapDispatchToProps)(EditMessage);
