import {SHOW_MODAL, HIDE_MODAL, SET_CURRENT_COMMENT_ID, DROP_CURRENT_COMMENT_ID} from './actionTypes';

const initialState = {
    messageId: '',
    isShown: false
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SHOW_MODAL:
            return {
                ...state,
                isShown: true
            }
        case HIDE_MODAL:
            return {
                ...state,
                isShown: false
            }
        case SET_CURRENT_COMMENT_ID:
            return {
                ...state,
                messageId: action.payload.id
            }
        case DROP_CURRENT_COMMENT_ID:
            return {
                ...state,
                messageId: ''
            }
        default:
            return state;
    }
};
