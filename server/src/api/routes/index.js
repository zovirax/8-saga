const messageRoute = require('./message.route');
const userRoute = require('./user.route');

module.exports = app => {
    app.use('/api/messages', messageRoute);
    app.use('/api/auth', userRoute);
}
