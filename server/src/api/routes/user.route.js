const {Router} = require('express');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const {secretKey, expiresIn} = require('../../config/jwt.config');
const authMiddleware = require('../middlewares/auth');

const router = new Router();

router.post('/login', passport.authenticate('login', {session: false}), (req, res) => {
    const {id, role} = req.user;
    res.send({user: req.user, token: jwt.sign({id, role}, secretKey, {expiresIn})});
});

router.get('/profile', authMiddleware, (req, res) => {
    res.send(req.user);
})

module.exports = router;
