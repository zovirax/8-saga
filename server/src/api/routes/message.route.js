const {Router} = require('express');
const messageService = require('../services/message.service');

const router = new Router();

router.get('/', (req, res) => {
    res.send(messageService.getAll());
});

router.post('/', (req, res) => {
    const {text, userId, user, createdAt, editedAt = '', id} = req.body;
    const newMessage = messageService.create({text, userId, user, createdAt, editedAt, id});
    res.send(newMessage);
})

router.delete('/:id', (req, res) => {
    const id = req.params.id;
    const deletedMessage = messageService.deleteById(id);
    res.send(deletedMessage);
})

router.put('/:id', (req, res) => {
    const id = req.params.id;
    const {text, userId, user, createdAt, editedAt = ''} = req.body;
    const updatedMessage = messageService.update(id, {text, userId, user, createdAt, editedAt, id});
    res.send(updatedMessage);
});

module.exports = router;
