const BaseService = require('./base.service');
const userRepo = require('../../db/repositories/userRepo');

class UserService extends BaseService {
    constructor() {
        super(userRepo);
    }

    getByUsername(user){
        return this.repo.getByUsername(user);
    }
}

module.exports = new UserService();
