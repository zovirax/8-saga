const BaseService = require('./base.service');
const messageRepo = require('../../db/repositories/messageRepo');

class MessageService extends BaseService {
    constructor() {
        super(messageRepo);
    }
}

module.exports = new MessageService();
