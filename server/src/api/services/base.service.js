class BaseService {
    constructor(repo) {
        this.repo = repo;
    }

    getById(id) {
        return this.search({id});
    }

    getAll() {
        return this.repo.getAll();
    }

    search(search) {
        return this.repo.getOne(search);
    }

    create(data) {
        return this.repo.create(data);
    }

    deleteById(id) {
        return this.repo.delete(id);
    }

    update(id, data) {
        this.repo.update(id, data);
        return this.getById(id);
    }
}

module.exports = BaseService;
