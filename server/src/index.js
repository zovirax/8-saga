const express = require('express');
const cors = require('cors');
const setupRoutes = require('./api/routes/index');
require('./config/passport.config');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
setupRoutes(app);

app.listen(5000, () => {
    console.log('Server is listening on 5000 port');
})

module.exports = app;
