const BaseRepo = require('./baseRepo');

class UserRepo extends BaseRepo {
    constructor() {
        super('users');
    }
    getByUsername(user){
        return this.getOne({user});
    }
}

module.exports = new UserRepo();
