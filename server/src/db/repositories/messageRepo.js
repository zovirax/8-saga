const BaseRepo = require('./baseRepo');

class MessageRepo extends BaseRepo {
    constructor() {
        super('messages');
    }
}

module.exports = new MessageRepo();
