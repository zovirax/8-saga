const { ExtractJwt } = require('passport-jwt');

const secretKey = 'secret';
const expiresIn = '24h';

module.exports = {
    jwtOptions: {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: secretKey,
    },
    secretKey,
    expiresIn,
};
