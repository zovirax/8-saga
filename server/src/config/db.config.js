const path = require('path');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const dbPath = `${path.resolve()}${path.sep}database.json`;

const adapter = new FileSync(dbPath);
const dbAdapter = low(adapter);
const defaultDb = {users: [], messages: []};
dbAdapter.defaults(defaultDb).write();

module.exports = dbAdapter;
