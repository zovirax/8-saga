const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const JwtStrategy = require('passport-jwt').Strategy;
const {jwtOptions} = require('./jwt.config');
const {unauthorized} = require('../helpers/types/HttpError');
const userService = require('../api/services/user.service');

passport.use('login',
    new LocalStrategy({
        usernameField: 'user',
    }, (username, password, done) => {
        try {
            const userByEmail = userService.getByUsername(username);
            if (!userByEmail) {
                return done(unauthorized('User with such username does not exist'), null);
            }
            const isMatched = password === userByEmail.password;
            return (isMatched ? done(null, userByEmail) : done(unauthorized('Password is wrong'), null));
        } catch (err) {
            return done(err);
        }
    }));

passport.use(new JwtStrategy(jwtOptions, async (jwtPayload, done) => {
    try {
        const {sub: id} = jwtPayload;
        const userById = userService.getById(id);
        done(null, userById);
    } catch (err) {
        done(err);
    }
}));
